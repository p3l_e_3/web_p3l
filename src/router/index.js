import VueRouter from 'vue-router'
import middleware from './middleware'
import http from '@/service'
import Cookies from 'js-cookie'
import store from '@/store'

// HOME
import BaseHome from '@/components/Home/base/Template'

import Homepage from '@/components/Home/pages/Homepage'
import Rooms from '@/components/Home/pages/Room'
import ReservationHome from '@/components/Home/pages/Reservation'
import Search from '@/components/Home/pages/Search'
import BookingHistory from '@/components/Home/pages/BookingHistory'

// APP
import Login from '@/components/App/pages/Login'
import BaseApp from '@/components/App/base/Template'

import Dashboard from '@/components/App/pages/Dashboard'
import User from '@/components/App/pages/User'
import Image from '@/components/App/pages/Image'
import Facility from '@/components/App/pages/Facility'
import PaidFacility from '@/components/App/pages/PaidFacility'
import RoomType from '@/components/App/pages/RoomType'
import Room from '@/components/App/pages/Room'
import Season from '@/components/App/pages/Season'
import CreditCard from '@/components/App/pages/CreditCard'
import AccountBank from '@/components/App/pages/AccountBank'
import Reservation from '@/components/App/pages/Reservation'
import Transaction from '@/components/App/pages/Transaction'

const routerGuard = (to, from, next) => {
  if (middleware.hasToken()) {
    http.setToken()
    store.dispatch('auth/validate')
    .then(res => {
      if(res) {
        if (middleware.hasRole(to)) {
          if (!middleware.matchRole(to, res)) {
            switch (middleware.getRole(res)) {
              case 1:
              case 2:
                next({ name: 'dashboard' })
                break;
              case 3:
                next({ name: 'homepage' })
                break;
            }
          }
        }
      }
    })
    .catch(err => {
      console.log('MASUK')
      if (err) {
        next({ name: 'login' })
      }
    })
  } else {
    if (middleware.hasRole(to)) {
      next({ name: 'login' })
    }
  }
  next()
}

const loginGuard = (to, from, next) => {
  if (middleware.hasToken()) {
    http.setToken()
    store.dispatch('auth/validate')
    .then(res => {
      if (res) {
        switch (middleware.getRole(res)) {
          case 1:
          case 2:
            next({ name: 'dashboard' })
            break;
          case 3:
            next({ name: 'homepage' })
            break;
        }    
      }
    })
  }
  next()
}

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/', 
      component: BaseHome,
      children: [
        {
          name: 'homepage', 
          path: '/',
          component: Homepage
        },
        {
          name: 'rooms', 
          path: '/room',
          component: Rooms
        },
        {
          name: 'search', 
          path: '/search',
          component: Search
        },
        {
          name: 'booking', 
          path: '/booking',
          component: ReservationHome
        },
        {
          name: 'booking-history', 
          path: '/booking-history',
          component: BookingHistory
        }
      ]
    },
    {
      path: '/admin',
      component: BaseApp,
      props: true,
      children: [
        { 
          name: 'dashboard', 
          path: '/admin',
          component: Dashboard ,
          meta: { role: [1] }
        },
        { 
          name: 'image', 
          path: '/admin/image',
          component: Image ,
          meta: { role: [1] }
        },
        {
          name: 'user', 
          path: '/admin/user',
          component: User,
          meta: { role: [1] }
        },
        {
          name: 'facility',
          path: '/admin/facility',
          component: Facility,
          meta: { role: [1] }
        },
        {
          name: 'paid_facility', 
          path: '/admin/paid_facility',
          component: PaidFacility,
          meta: { role: [1] }
        },
        {
          name: 'room_type', 
          path: '/admin/room_type',
          component: RoomType,
          meta: { role: [1] }
        },
        {
          name: 'room', 
          path: '/admin/room',
          component: Room,
          meta: { role: [1] }
        },
        {
          name: 'season', 
          path: '/admin/season',
          component: Season,
          meta: { role: [1] }
        },
        {
          name: 'credit_card', 
          path: '/admin/credit_card',
          component: CreditCard,
          meta: { role: [1] }
        },
        {
          name: 'account_bank', 
          path: '/admin/account_bank',
          component: AccountBank,
          meta: { role: [1] }
        },
        {
          name: 'reservation', 
          path: '/admin/reservation',
          component: Reservation,
          meta: { role: [1] }
        },
        {
          name: 'transaction', 
          path: '/admin/transaction',
          component: Transaction,
          meta: { role: [1] }
        },
      ]
    },
    { 
      name: 'login', 
      path: '/login', 
      component: Login,
      beforeEnter: loginGuard
    }
  ]
})

router.beforeEach(routerGuard)

export default router 