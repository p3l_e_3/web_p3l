import http from '@/service'
import Cookies from 'js-cookie'

export default {
  getRole(user) {
    return user !== null ? user.role.id : null
  },
  hasToken() {
    return Cookies.get('auth') !== undefined ? true : false
  },
  hasRole(route) {
    return route.meta.role !== undefined ? true : false
  },
  matchRole(route, user) {
    return route.meta.role.indexOf(this.getRole(user)) != -1 ? true : false
  }
}