import axios from 'axios'
import Cookies from 'js-cookie'

export default {
  init() {
    axios.defaults.baseURL = 'http://127.0.0.1:8000/api/'
  },
  setToken() {  
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + (Cookies.get('auth') !== undefined ? Cookies.get('auth') : '' )
  },
  removeToken() {  
    axios.defaults.headers.common['Authorization'] = ''
  },
  get(url, successCallback, errorCallback) {
    return axios({
      method: 'get',
      url,
    }).then(successCallback)
      .catch(errorCallback)
  }, 
  post(url, data, successCallback, errorCallback) {
    return axios({
      method: 'post',
      url,
      data
    }).then(successCallback)
      .catch(errorCallback)
  },
  patch(url, data, successCallback, errorCallback) {
    return axios({
      method: 'patch',
      url,
      data
    }).then(successCallback)
      .catch(errorCallback)
  },
  delete(url, successCallback, errorCallback) {
    return axios({
      method: 'delete',
      url
    }).then(successCallback)
      .catch(errorCallback)
  }
}