import http from '../service'
import Cookies from 'js-cookie'

const auth = {
  namespaced: true,
  state: {
    user: {
      username: ''
    }
  },
  mutations: {
    setUser(state, source) {
      state.user = source
    },
    removeUser(state) {
      state.user = null
    },
    setToken(state, source) {
      Cookies.set('auth', source)
      http.setToken()
    },
    removeToken(state) {
      Cookies.remove('auth')
      http.removeToken()
    }
  },
  actions: {
    validate(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setUser', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          context.commit('removeToken')
          reject(errData.response)
        }
        http.get('info', successCallback, errorCallback)
      })
    },
    login(context, payload) {
      return new Promise((resolve, reject) => {
        const data = {
          username: payload.username,
          password: payload.password
        }
        const successCallback = res => {
          if (res.status === 200) {
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.post('login', data, successCallback, errorCallback)
      })
    },
    logout(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            console.log(res.status)
            context.commit('removeToken')
            context.commit('removeUser')
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.post('logout', null, successCallback, errorCallback)
      })
    }
  }

}

export default auth