import http from '../service'

const changeFormatDate = (str) => {
    if (str != null) {
      return str.substring(2, 4) + '-' + str.substring(5, 7);
    }
    return '';
}

const credit_card = {
  namespaced: true,
  state: {
    data: []
  },
  mutations: {
    setSource(state, source) {
      state.data = source
    }
  },
  actions: {
    index(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('credit_card', successCallback, errorCallback)
      })
    },
    store(context, payload) {
      return new Promise((resolve, reject) => {
        const data = {
          account_number: payload.account_number,
          account_name: payload.account_name,
          type: payload.type,
          expiry: changeFormatDate(payload.expiry),
          vcc: payload.vcc
        }
        
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.post('credit_card', data, successCallback, errorCallback)
      })
    },
    update(context, payload) {
      return new Promise((resolve, reject) => {
        const data = {
          account_name: payload.account_name,
          type: payload.type,
          expiry: changeFormatDate(payload.expiry),
          vcc: payload.vcc
        }

        const id = payload.id
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.patch(`credit_card/${id}`, data, successCallback, errorCallback)
      })
    },
    delete(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.delete(`credit_card/${id}`, successCallback, errorCallback)
      })
    }
  }
}

export default credit_card