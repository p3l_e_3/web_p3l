import Vuex from 'vuex';

import auth from './auth'
import image from './image'
import user from './user'
import role from './role'
import guest from './guest'
import branch from './branch'
import facility from './facility'
import paid_facility from './paid_facility'
import room from './room'
import season from './season'
import room_type from './room_type'
import bed_type from './bed_type'
import account_bank from './account_bank'
import credit_card from './credit_card'
import reservation from './reservation'
import report from './report'
import transaction from './transaction'

const store = new Vuex.Store({
 modules: {
   auth,
   role,
   user,
   guest,
   image,
   facility,
   paid_facility,
   branch,
   facility,
   room,
   season,
   room_type,
   bed_type,
   account_bank,
   credit_card,
   reservation,
   report,
   transaction,
 }
})

export default store