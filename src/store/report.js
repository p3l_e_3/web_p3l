import http from '../service'

const report = {
  namespaced: true,
  state: {
    data1: [],
    data2: [],
    data3: [],
    data4: [],
    data5: []
  },
  mutations: {
    setSource1(state, source) {
      state.data1 = source
    },
    setSource2(state, source) {
      state.data2 = source
    },
    setSource3(state, source) {
      state.data3 = source
    },
    setSource4(state, source) {
      state.data4 = source
    },
    setSource5(state, source) {
      state.data5 = source
    },
  },
  actions: {
    tamuTahunan(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource1', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('reports/report1', successCallback, errorCallback)
      })
    },
    tamuPerJenisKamar(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource2', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('reports/report4', successCallback, errorCallback)
      })
    },
    pendapatanBulanan(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource3', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('reports/report2', successCallback, errorCallback)
      })
    },
    pendapatanTahunan(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource4', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('reports/report3', successCallback, errorCallback)
      })
    },
    top5Customer(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource5', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('reports/report5', successCallback, errorCallback)
      })
    }
  }
}

export default report
