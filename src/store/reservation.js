import http from '../service'

const reservation = {
  namespaced: true,
  state: {
    data: []
  },
  mutations: {
    setSource(state, source) {
      state.data = source
    }
  },
  actions: {
    indexForTransaction(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            var temp = [];
            res.data.forEach(item => {
              if (item.transaction == null) {
                temp.push(item)
              }
            });
            context.commit('setSource', temp)
            resolve(temp)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('reservation', successCallback, errorCallback)
      })
    },
    index(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('reservation', successCallback, errorCallback)
      })
    },
    store(context, payload) {
      console.log(payload)
      return new Promise((resolve, reject) => {
        const data = {
          adult: payload.adult,
          child: payload.child,
          start_date: payload.start_date,
          end_date: payload.end_date,
          type: payload.type,
          guest: payload.guest,
          rooms: payload.rooms,
          payment: payload.payment
        }
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.post('reservation', data, successCallback, errorCallback)
      })
    },
    update(context, payload) {
      return new Promise((resolve, reject) => {
        console.log(payload);
        const data = {
          adult: payload.adult,
          child: payload.child,
          start_date: payload.start_date,
          end_date: payload.end_date,
          type: payload.type,
          guest: payload.guest,
          room: payload.room,
          payment: payload.payment
        }

        const id = payload.id
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.patch(`reservation/${id}`, data, successCallback, errorCallback)
      })
    },
    delete(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.delete(`reservation/${id}`, successCallback, errorCallback)
      })
    }
  }
}

export default reservation