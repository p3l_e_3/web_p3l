import http from '../service'

const room = {
  namespaced: true,
  state: {
    data: []
  },
  mutations: {
    setSource(state, source) {
      state.data = source
    }
  },
  actions: {
    index(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('room', successCallback, errorCallback)
      })
    },
    available(context, payload) {
      return new Promise((resolve, reject) => {
        const data = {
          start_date: payload.start_date,
          end_date: payload.end_date,
        }
        console.log(data)
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.post('room/available', data, successCallback, errorCallback)
      })
    },
    store(context, payload) {
      return new Promise((resolve, reject) => {
        const data = {
          floor: payload.floor,
          number: payload.number,
          smoking: payload.smoking,
          room_type_code: payload.room_type.code,
          room_bed_id: payload.room_bed.id,
          branch_id: payload.branch.id,
        }
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.post('room', data, successCallback, errorCallback)
      })
    },
    update(context, payload) {
      return new Promise((resolve, reject) => {
        console.log(payload)
        const data = {
          smoking: payload.smoking,
          status: payload.status,
          branch_id: payload.branch.id,
          room_bed_id: payload.room_bed.id,
        }
        const code = payload.code
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.patch(`room/${code}`, data, successCallback, errorCallback)
      })
    },
    delete(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.delete(`room/${id}`, successCallback, errorCallback)
      })
    }
  }
}

export default room