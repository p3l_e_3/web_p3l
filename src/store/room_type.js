import http from '../service'

const room_type = {
  namespaced: true,
  state: {
    data: []
  },
  mutations: {
    setSource(state, source) {
      state.data = source
    }
  },
  actions: {
    index(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('room_type', successCallback, errorCallback)
      })
    },
    store(context, payload) {
      return new Promise((resolve, reject) => {
        const bedType = []
        payload.bed_types.forEach(item => {
          bedType.push({
            bed_type_id: item.pivot.id,
            qty: item.pivot.qty
          })
        })

        const data = {
          code: payload.code,
          name: payload.name,
          size: payload.size,
          max_guests: payload.max_guests,
          price: payload.price,
          description: payload.description,
          facilities: payload.facilities,
          bed_types: bedType,
        }
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.post('room_type', data, successCallback, errorCallback)
      })
    },
    update(context, payload) {
      return new Promise((resolve, reject) => {
        const bedType = []
        payload.bed_types.forEach(item => {
          bedType.push({
            bed_type_id: item.id,
            qty: item.pivot.qty
          })
        })

        const facilities = []
        payload.facilities.forEach(item => {
          facilities.push(item.id)
          console.log(facilities)
        })
        
        const data = {
          size: payload.size,
          max_guests: payload.max_guests,
          price: payload.price,
          description: payload.description,
          facilities: facilities,
          bed_types: bedType,
        }
        
        const id = payload.code
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
            .then(res => {
              resolve(res.data)
            })
            .catch(err => {
              console.log(err)
            })
          }
        }
        const errorCallback = err => {
          console.log(data)
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.patch(`room_type/${id}`, data, successCallback, errorCallback)
      })
    },
    delete(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.delete(`room_type/${id}`, successCallback, errorCallback)
      })
    }
  }
}

export default room_type