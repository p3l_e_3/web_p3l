import http from '../service'

const season = {
  namespaced: true,
  state: {
    data: []
  },
  mutations: {
    setSource(state, source) {
      state.data = source
    }
  },
  actions: {
    index(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('season', successCallback, errorCallback)
      })
    },
    store(context, payload) {
      return new Promise((resolve, reject) => {

        const dataRoomType = []
        payload.room_types.forEach(item => {
          dataRoomType.push({
            room_type_code: item.pivot.code,
            type: item.pivot.type,
            amount: item.pivot.amount,
          })
        })

        const data = {
          name: payload.name,
          start_date: payload.start_date,
          end_date: payload.end_date,
          room_type: dataRoomType
        }
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.post('season', data, successCallback, errorCallback)
      })
    },
    update(context, payload) {
      return new Promise((resolve, reject) => {
        const dataRoomType = []
        payload.room_types.forEach(item => {
          dataRoomType.push({
            room_type_code: item.pivot.code,
            type: item.pivot.type,
            amount: item.pivot.amount,
          })
        })

        const data = {
          name: payload.name,
          start_date: payload.start_date,
          end_date: payload.end_date,
          room_type: dataRoomType
        }
        

        const id = payload.id
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.patch(`season/${id}`, data, successCallback, errorCallback)
      })
    },
    delete(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.delete(`season/${id}`, successCallback, errorCallback)
      })
    }
  }
}

export default season