import http from '../service'


const transaction = {
  namespaced: true,
  state: {
    data: []
  },
  mutations: {
    setSource(state, source) {
      state.data = source
    }
  },
  actions: {
    index(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('transaction', successCallback, errorCallback)
      })
    },
    store(context, payload) {
      return new Promise((resolve, reject) => {
        const data = {
          reservation_id: payload.reservation_id,
          facilities: payload.facilities
        }
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.post('transaction', data, successCallback, errorCallback)
      })
    },
    update(context, payload) {
      console.log(payload)
      return new Promise((resolve, reject) => {
        const data = {}
        if (payload.check_out == true) {
          data.check_out = payload.check_out
          data.payment = payload.payment
        }

        data.facilities = payload.facilities
        const id = payload.id
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                console.log(err)
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.patch(`transaction/${id}`, data, successCallback, errorCallback)
      })
    }
  }
}

export default transaction
