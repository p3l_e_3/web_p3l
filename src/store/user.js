import http from '../service'

const user = {
  namespaced: true,
  state: {
    data: []
  },
  mutations: {
    setSource(state, source) {
      state.data = source
    }
  },
  actions: {
    index(context) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.commit('setSource', res.data)
            resolve(res.data)
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.get('user', successCallback, errorCallback)
      })
    },
    store(context, payload) {
      return new Promise((resolve, reject) => {
        const data = {
          username: payload.username,
          password: payload.password,
          email: payload.email,
          role_id: payload.role.id
        }
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                reject()
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.post('user', data, successCallback, errorCallback)
      })
    },
    update(context, payload) {
      return new Promise((resolve, reject) => {
        const data = {
          username: payload.username,
          password: payload.password,
          email: payload.email,
          role_id: payload.role.id
        }

        const id = payload.id
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                reject()
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.patch(`user/${id}`, data, successCallback, errorCallback)
      })
    },
    delete(context, id) {
      return new Promise((resolve, reject) => {
        const successCallback = res => {
          if (res.status === 200) {
            context.dispatch('index')
              .then(res => {
                resolve(res.data)
              })
              .catch(err => {
                reject()
              })
          }
        }
        const errorCallback = err => {
          const errData = Object.assign({}, err)
          reject(errData.response)
        }
        http.delete(`user/${id}`, successCallback, errorCallback)
      })
    }
  }
}

export default user